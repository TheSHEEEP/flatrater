package eu.jdrabner.test.flatrater;

/**
 * Created by thesh on 01/01/2018.
 * Flat data class.
 */

public class FlatData {
    public String   address = "";
    public Integer  floor = 1;
    public Double   price = 1.0;
    public Double   size  = 50.0;
    public Double   rentDeposit = 1500.0;
    public Integer  rooms = 2;
    public Integer  numNeighbors = 2;
    public Integer  wallNoiseProtection = 5;
    public Integer  airQuality = 5;
    public Integer  liftRating = 5;
    public Integer  outsideNoise = 5;
    public Integer  windowNoiseProtection = 5;
    public Integer  kitchenEquipment = 5;
    public Integer  socketDistribution = 5;
    public Integer  internetSpeed = 5;
    public Integer  busConnection = 5;
    public Integer  storeDistance = 5;
    public Integer  roomLayout = 5;
    public Integer  windowBlinds = 5;
    public Integer  surroundings = 5;

}
