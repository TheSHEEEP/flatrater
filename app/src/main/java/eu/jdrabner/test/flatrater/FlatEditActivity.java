package eu.jdrabner.test.flatrater;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import eu.jdrabner.test.flatrater.util.MultiUseTextWatcher;

public class FlatEditActivity extends AppCompatActivity {

    private FlatRaterContext context = null;

    private Integer flatIndex = -1;

    private EditText    editAddress = null;
    private EditText    editFloor = null;
    private EditText    editSize = null;
    private EditText    editRooms = null;
    private EditText    editNeighbors = null;
    private EditText    editPrice = null;
    private EditText    editDeposit = null;
    private SeekBar     barWallNoiseProt = null;
    private SeekBar     barAirQuality = null;
    private SeekBar     barLiftRating = null;
    private SeekBar     barOutsideNoise = null;
    private SeekBar     barWindowNoiseProtection = null;
    private SeekBar     barKitchenEquip = null;
    private SeekBar     barSocketDistribution = null;
    private SeekBar     barInternetSpeed = null;
    private SeekBar     barBusConnection = null;
    private SeekBar     barStoreDistance = null;
    private SeekBar     barRoomLayout = null;
    private SeekBar     barWindowBlinds = null;
    private SeekBar     barSurroundings = null;
    private Button      btnRemove = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flat_edit);

        // Get the context
        context = (FlatRaterContext)getApplicationContext();

        // Get the index
        if (getIntent().getExtras() == null) {
            Log.e("FlatEditActivity", "Creating activity without parameter.");
            return;
        }
        flatIndex = getIntent().getExtras().getInt("flatIndex");

        // Fill the UI
        FlatData data = context.flats.get(flatIndex);
        editAddress = findViewById(R.id.editAddress);
        editAddress.setText(data.address);
        editFloor = findViewById(R.id.editFloor);
        editFloor.setText(data.floor.toString());
        editSize = findViewById(R.id.editSize);
        editSize.addTextChangedListener(new MultiUseTextWatcher(" m²", true, editSize));
        editSize.setText(data.size.toString());
        editRooms = findViewById(R.id.editRooms);
        editRooms.setText(data.rooms.toString());
        editNeighbors = findViewById(R.id.editNeighbors);
        editNeighbors.setText(data.numNeighbors.toString());
        editPrice = findViewById(R.id.editPrice);
        editPrice.addTextChangedListener(new MultiUseTextWatcher(" €", true, editPrice));
        editPrice.setText(data.price.toString());
        editDeposit = findViewById(R.id.editDeposit);
        editDeposit.addTextChangedListener(new MultiUseTextWatcher(" €", true, editDeposit));
        editDeposit.setText(data.rentDeposit.toString());
        barWallNoiseProt = findViewById(R.id.barWallNoise);
        barWallNoiseProt.setProgress(data.wallNoiseProtection);
        barAirQuality = findViewById(R.id.barAirQuality);
        barAirQuality.setProgress(data.airQuality);
        barLiftRating = findViewById(R.id.barLiftRating);
        barLiftRating.setProgress(data.liftRating);
        barOutsideNoise = findViewById(R.id.barOutsideNoise);
        barOutsideNoise.setProgress(data.outsideNoise);
        barWindowNoiseProtection = findViewById(R.id.barWindowNoiseProt);
        barWindowNoiseProtection.setProgress(data.windowNoiseProtection);
        barKitchenEquip = findViewById(R.id.barKitchenEquip);
        barKitchenEquip.setProgress(data.kitchenEquipment);
        barSocketDistribution = findViewById(R.id.barSocketDist);
        barSocketDistribution.setProgress(data.socketDistribution);
        barInternetSpeed = findViewById(R.id.barInternetSpeed);
        barInternetSpeed.setProgress(data.internetSpeed);
        barBusConnection = findViewById(R.id.barBusConnection);
        barBusConnection.setProgress(data.busConnection);
        barStoreDistance = findViewById(R.id.barStoreDistance);
        barStoreDistance.setProgress(data.storeDistance);
        barRoomLayout = findViewById(R.id.barRoomLayout);
        barRoomLayout.setProgress(data.roomLayout);
        barWindowBlinds = findViewById(R.id.barWindowBlinds);
        barWindowBlinds.setProgress(data.windowBlinds);
        barSurroundings = findViewById(R.id.barSurroundings);
        barSurroundings.setProgress(data.surroundings);
        btnRemove = findViewById(R.id.btnDelete);

        // Attach the listeners that will cause changes to be save
        // EditTexts
        View.OnFocusChangeListener focusChangeListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    storeValuesToFlat();
                }
            }
        };
        EditText.OnEditorActionListener actionListener = new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE || i == EditorInfo.IME_ACTION_SEARCH ||
                        (keyEvent != null &&
                        (keyEvent.getAction() == KeyEvent.ACTION_DOWN || keyEvent.getAction() == KeyEvent.KEYCODE_ENTER))) {
                    if (keyEvent == null || !keyEvent.isShiftPressed()) {
                        storeValuesToFlat();
                        return true;
                    }
                }
                return false;
            }
        };
        editAddress.setOnFocusChangeListener(focusChangeListener);
        editAddress.setOnEditorActionListener(actionListener);
        editFloor.setOnFocusChangeListener(focusChangeListener);
        editFloor.setOnEditorActionListener(actionListener);
        editSize.setOnFocusChangeListener(focusChangeListener);
        editSize.setOnEditorActionListener(actionListener);
        editRooms.setOnFocusChangeListener(focusChangeListener);
        editRooms.setOnEditorActionListener(actionListener);
        editNeighbors.setOnFocusChangeListener(focusChangeListener);
        editNeighbors.setOnEditorActionListener(actionListener);
        editPrice.setOnFocusChangeListener(focusChangeListener);
        editPrice.setOnEditorActionListener(actionListener);
        editDeposit.setOnFocusChangeListener(focusChangeListener);
        editDeposit.setOnEditorActionListener(actionListener);
        // SeekBars
        SeekBar.OnSeekBarChangeListener seekBarListener = new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                storeValuesToFlat();

                // Hide the keyboard as we clearly don't need it right now
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(seekBar.getWindowToken(), 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        };
        barWallNoiseProt.setOnSeekBarChangeListener(seekBarListener);
        barAirQuality.setOnSeekBarChangeListener(seekBarListener);
        barLiftRating.setOnSeekBarChangeListener(seekBarListener);
        barOutsideNoise.setOnSeekBarChangeListener(seekBarListener);
        barWindowNoiseProtection.setOnSeekBarChangeListener(seekBarListener);
        barKitchenEquip.setOnSeekBarChangeListener(seekBarListener);
        barSocketDistribution.setOnSeekBarChangeListener(seekBarListener);
        barInternetSpeed.setOnSeekBarChangeListener(seekBarListener);
        barBusConnection.setOnSeekBarChangeListener(seekBarListener);
        barStoreDistance.setOnSeekBarChangeListener(seekBarListener);
        barRoomLayout.setOnSeekBarChangeListener(seekBarListener);
        barWindowBlinds.setOnSeekBarChangeListener(seekBarListener);
        barSurroundings.setOnSeekBarChangeListener(seekBarListener);

        // Remove button
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                // Remove this flat
                                context.flats.remove((int)flatIndex);
                                context.storeData();
                                flatIndex = -1;

                                // Move to selection activity
                                Intent intent = new Intent(FlatEditActivity.this, FlatSelectionActivity.class);
                                startActivity(intent);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };

                // Show certainty dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setMessage("Are you sure you want to remove this flat?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });

        // Listener that hides keyboard when clicking anywhere
        View.OnClickListener keyboardHideListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Hide the keyboard as we clearly don't need it right now
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        };
        LinearLayout mainLayout = findViewById(R.id.mainEditLayout);
        LinearLayout scrollLayout = findViewById(R.id.editScrollLayout);
        mainLayout.setOnClickListener(keyboardHideListener);
        scrollLayout.setOnClickListener(keyboardHideListener);

        // Set the title
        setTitle(String.format("FlatRater - Edit Flat (Rating: %.2f)", context.weighting.getRatingForFlat(data)));
    }

    @Override
    protected void onStop() {
        super.onStop();

        storeValuesToFlat();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        storeValuesToFlat();
    }

    /**
     * Stores all current values to the flat.
     */
    private void storeValuesToFlat() {

        // Don't do anything with an invalid id
        if (flatIndex == -1) {
            return;
        }

        // Store all values to the flat
        FlatData data = new FlatData();
        data.address = editAddress.getText().toString();
        data.floor = Integer.valueOf(editFloor.getText().toString());
        data.size = Double.valueOf(editSize.getText().toString().replace(" m²", ""));
        data.rooms = Integer.valueOf(editRooms.getText().toString());
        data.price = Double.valueOf(editPrice.getText().toString().replace(" €", ""));
        data.rentDeposit = Double.valueOf(editDeposit.getText().toString().replace(" €", ""));
        data.numNeighbors = Integer.valueOf(editNeighbors.getText().toString());
        data.wallNoiseProtection = barWallNoiseProt.getProgress();
        data.airQuality = barAirQuality.getProgress();
        data.outsideNoise = barOutsideNoise.getProgress();
        data.windowNoiseProtection = barWindowNoiseProtection.getProgress();
        data.liftRating = barLiftRating.getProgress();
        data.kitchenEquipment = barKitchenEquip.getProgress();
        data.socketDistribution = barSocketDistribution.getProgress();
        data.internetSpeed = barInternetSpeed.getProgress();
        data.busConnection = barBusConnection.getProgress();
        data.storeDistance = barStoreDistance.getProgress();
        data.roomLayout = barRoomLayout.getProgress();
        data.windowBlinds = barWindowBlinds.getProgress();
        data.surroundings = barSurroundings.getProgress();
        context.flats.set(flatIndex, data);

        // Store to JSON
        context.storeData();

        // Set the title
        setTitle(String.format("FlatRater - Edit Flat (Rating: %.2f)", context.weighting.getRatingForFlat(data)));
    }
}
