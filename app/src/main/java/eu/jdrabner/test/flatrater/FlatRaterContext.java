package eu.jdrabner.test.flatrater;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import eu.jdrabner.test.flatrater.util.FlatDataWeighting;

/**
 * Created by thesh on 01/01/2018.
 */
public class FlatRaterContext extends Application {

    static String flatFileName = "/flats.json";
    static String weightingFileName = "/weighting.json";

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private ContextWrapper context = null;

    public ArrayList<FlatData>  flats = new ArrayList<>();
    public FlatDataWeighting weighting = new FlatDataWeighting();

    /**
     * Initialize the entire context.
     */
    public void initialize() {
        // Get context
        context = new ContextWrapper(getApplicationContext());

        // Initialize flats
        initFlats();

        // Initialize rating weighting
        initWeighting();
    }

    /**
     * Stores the currently saved flats and data weightings.
     */
    public void storeData() {
        // Store flats
        storeFlats();

        // Store weighting
        storeWeighting();
    }

    /**
     * Make sure the activity has all needed rights.
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    /**
     * Init the flats.
     */
    private void initFlats() {
        // Open the file
        File jsonFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)+ "/FlatRater" + flatFileName);
        jsonFile.getParentFile().mkdirs();
        // If it doesn't exist, create it and have an empty flat list
        if (!jsonFile.exists()) {
            try {
                jsonFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // If it exists, open it and parse into our ArrayList
        else {
            try {
                FileInputStream is = new FileInputStream(jsonFile);
                int size = is.available();

                // Only fill from file if there is anything in it
                if (size > 0) {
                    byte[] buffer = new byte[size];
                    is.read(buffer);
                    is.close();

                    String json = new String(buffer);
                    JsonParser parser = new JsonParser();
                    JsonElement root =  parser.parse(json);
                    JsonArray array = root.getAsJsonArray();
                    Gson gson = new Gson();
                    flats = gson.fromJson(array, new TypeToken<ArrayList<FlatData>>(){}.getType());
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            catch (JsonParseException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Store the flats to JSON.
     */
    private void storeFlats() {
        // Open the file
        File jsonFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)+ "/FlatRater" + flatFileName);
        jsonFile.getParentFile().mkdirs();
        try {
            jsonFile.createNewFile();
            FileOutputStream os = new FileOutputStream(jsonFile, false);

            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(flats);
            os.write(json.getBytes());
            os.flush();
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Init rating weighting.
     */
    private void initWeighting() {
        // Open the file
        File jsonFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)+ "/FlatRater" + weightingFileName);
        jsonFile.getParentFile().mkdirs();
        // If it doesn't exist, create it
        if (!jsonFile.exists()) {
            try {
                jsonFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // If it exists, open it and parse into our weighting
        else {
            try {
                FileInputStream is = new FileInputStream(jsonFile);
                int size = is.available();

                // Only fill from file if there is anything in it
                if (size > 0) {
                    byte[] buffer = new byte[size];
                    is.read(buffer);
                    is.close();

                    String json = new String(buffer);
                    JsonParser parser = new JsonParser();
                    JsonElement root =  parser.parse(json);
                    Gson gson = new Gson();
                    weighting = gson.fromJson(root, FlatDataWeighting.class);
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            catch (JsonParseException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Store the weighting to JSON.
     */
    private void storeWeighting() {
        // Open the file
        File jsonFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)+ "/FlatRater" + weightingFileName);
        jsonFile.getParentFile().mkdirs();
        try {
            jsonFile.createNewFile();
            FileOutputStream os = new FileOutputStream(jsonFile, false);

            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(weighting);
            os.write(json.getBytes());
            os.flush();
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return;
    }
}
