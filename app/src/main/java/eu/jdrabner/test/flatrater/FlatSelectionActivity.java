package eu.jdrabner.test.flatrater;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import eu.jdrabner.test.flatrater.util.FlatDataAdapter;

public class FlatSelectionActivity extends AppCompatActivity {

    private FlatRaterContext raterContext = null;

    private ListView    flatList = null;
    private Button      btnAddFlat = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flat_selection);

        // Get context
        raterContext = (FlatRaterContext)getApplicationContext();

        // Set title
        setTitle("FlatRater - Flats");

        // Fill and connect list
        flatList = findViewById(R.id.flatList);
        FlatDataAdapter adapter = new FlatDataAdapter(FlatSelectionActivity.this, 0, raterContext.flats, raterContext.weighting);
        flatList.setAdapter(adapter);
        flatList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                // Move to edit activity
                Intent intent = new Intent(FlatSelectionActivity.this, FlatEditActivity.class);
                intent.putExtra("flatIndex", index);
                startActivity(intent);
            }
        });

        // Connect buttons
        btnAddFlat = findViewById(R.id.btnAddFlat);
        btnAddFlat.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // Create new flat
                FlatData data = new FlatData();
                raterContext.flats.add(data);

                // Move to edit activity
                Intent intent = new Intent(FlatSelectionActivity.this, FlatEditActivity.class);
                intent.putExtra("flatIndex", raterContext.flats.size() - 1);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshUI();
    }

    /**
     * Refreshes the data to redraw the UI.
     */
    private void refreshUI() {
        FlatDataAdapter adapter = (FlatDataAdapter)flatList.getAdapter();
        adapter.update(raterContext.flats, raterContext.weighting);
        flatList.invalidateViews();
    }
}
