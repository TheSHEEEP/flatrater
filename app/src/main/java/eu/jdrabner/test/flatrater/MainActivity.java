package eu.jdrabner.test.flatrater;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private FlatRaterContext raterContext = null;

    private Button btnSelect = null;
    private Button btnWeights = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        raterContext = (FlatRaterContext)getApplicationContext();

        // Only initialize if we have all the rights
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            raterContext.verifyStoragePermissions(this);
        }
        // We have the rights, so continue to initialize
        else {
            initInternal();
        }

        // Set title
        setTitle("FlatRater");
    }

    @Override // android recommended class to handle permissions
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // Continue once granted permission
                    initInternal();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.uujm
                    Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();

                    //app cannot function without this permission for now so close it...
                    onDestroy();
                }
                return;
            }

            // other 'case' line to check for other
            // permissions this app might request
        }
    }

    private void initInternal() {
        // Initialize context
        raterContext.initialize();

        // Connect buttons
        btnSelect = findViewById(R.id.btnSelectFlat);
        btnSelect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FlatSelectionActivity.class);
                startActivity(intent);
            }
        });
        btnWeights = findViewById(R.id.btnRatingWeights);
        btnWeights.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RatingWeightActivity.class);
                startActivity(intent);
            }
        });
    }
}
