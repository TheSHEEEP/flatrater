package eu.jdrabner.test.flatrater;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;

import eu.jdrabner.test.flatrater.util.RatingWeightChangeListener;

/**
 * Activity to set the rating weights.
 */
public class RatingWeightActivity extends AppCompatActivity {

    private FlatRaterContext context = null;

    private Button      btnWhatIs = null;
    private RatingBar   ratingSize = null;
    private RatingBar   ratingRooms = null;
    private RatingBar   ratingWallProt = null;
    private RatingBar   ratingWindowProt = null;
    private RatingBar   ratingOutsideNoise = null;
    private RatingBar   ratingNeighbors = null;
    private RatingBar   ratingSocketDist = null;
    private RatingBar   ratingInternetSpeed = null;
    private RatingBar   ratingBusConnection = null;
    private RatingBar   ratingStoreDistance = null;
    private RatingBar   ratingLiftBig = null;
    private RatingBar   ratingKitchenSize = null;
    private RatingBar   ratingRentDeposit = null;
    private RatingBar   ratingAirQuality = null;
    private RatingBar   ratingRoomLayout = null;
    private RatingBar   ratingWindowBlinds = null;
    private RatingBar   ratingSurroundings = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_weight);

        // Get context
        context = (FlatRaterContext)getApplicationContext();

        // Set title
        setTitle("FlatRater - Rating Weights");

        // Connect buttons
        btnWhatIs = findViewById(R.id.btnWhatIsThis);
        btnWhatIs.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "The weight of each rating determines how important that rating is for the total score.\n\nA rating with a 5-star weight will be 5 times more important than a rating with a 1-star weight.", Toast.LENGTH_LONG).show();
            }
        });
        ratingSize = findViewById(R.id.ratingSize);
        ratingSize.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "sizeWeight"));
        ratingRentDeposit = findViewById(R.id.ratingDeposit);
        ratingRentDeposit.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "rentDepositWeight"));
        ratingRooms = findViewById(R.id.ratingRooms);
        ratingRooms.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "roomsWeight"));
        ratingNeighbors = findViewById(R.id.ratingNeighbors);
        ratingNeighbors.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "neighborWeight"));
        ratingWallProt = findViewById(R.id.ratingWallProt);
        ratingWallProt.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "wallProtectionWeight"));
        ratingAirQuality = findViewById(R.id.ratingAirQual);
        ratingAirQuality.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "airQualityWeight"));
        ratingLiftBig = findViewById(R.id.ratingLiftBig);
        ratingLiftBig.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "liftBigWeight"));
        ratingOutsideNoise = findViewById(R.id.ratingOutsideNoise);
        ratingOutsideNoise.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "outsideNoiseWeight"));
        ratingWindowProt = findViewById(R.id.ratingWindowProt);
        ratingWindowProt.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "windowNoiseWeight"));
        ratingKitchenSize = findViewById(R.id.ratingKitchenSize);
        ratingKitchenSize.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "kitchenEquipmentWeight"));
        ratingSocketDist = findViewById(R.id.ratingSocketDist);
        ratingSocketDist.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "socketDistWeight"));
        ratingInternetSpeed = findViewById(R.id.ratingInternetSpeed);
        ratingInternetSpeed.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "internetSpeedWeight"));
        ratingBusConnection = findViewById(R.id.ratingBusConnection);
        ratingBusConnection.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "busConnectionWeight"));
        ratingStoreDistance = findViewById(R.id.ratingStoreDistance);
        ratingStoreDistance.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "storeDistanceWeight"));
        ratingRoomLayout = findViewById(R.id.ratingRoomLayout);
        ratingRoomLayout.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "roomLayoutWeight"));
        ratingWindowBlinds = findViewById(R.id.ratingWindowBlinds);
        ratingWindowBlinds.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "windowBlindsWeight"));
        ratingSurroundings = findViewById(R.id.ratingSurroundings);
        ratingSurroundings.setOnRatingBarChangeListener(new RatingWeightChangeListener(context, "surroundingsWeight"));

        // Apply to UI
        applyWeightingToUI();
    }

    /**
     * Apply stored values to UI.
     */
    private void applyWeightingToUI() {
        ratingSize.setRating(context.weighting.sizeWeight);
        ratingRentDeposit.setRating(context.weighting.rentDepositWeight);
        ratingRooms.setRating(context.weighting.roomsWeight);
        ratingNeighbors.setRating(context.weighting.neighborWeight);
        ratingWallProt.setRating(context.weighting.wallProtectionWeight);
        ratingAirQuality.setRating(context.weighting.airQualityWeight);
        ratingLiftBig.setRating(context.weighting.liftBigWeight);
        ratingOutsideNoise.setRating(context.weighting.outsideNoiseWeight);
        ratingWindowProt.setRating(context.weighting.windowNoiseWeight);
        ratingKitchenSize.setRating(context.weighting.kitchenEquipmentWeight);
        ratingSocketDist.setRating(context.weighting.socketDistWeight);
        ratingInternetSpeed.setRating(context.weighting.internetSpeedWeight);
        ratingBusConnection.setRating(context.weighting.busConnectionWeight);
        ratingStoreDistance.setRating(context.weighting.storeDistanceWeight);
        ratingRoomLayout.setRating(context.weighting.roomLayoutWeight);
        ratingWindowBlinds.setRating(context.weighting.windowBlindsWeight);
        ratingSurroundings.setRating(context.weighting.surroundingsWeight);
    }
}
