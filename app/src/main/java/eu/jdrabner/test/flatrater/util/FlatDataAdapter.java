package eu.jdrabner.test.flatrater.util;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.graphics.ColorUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import eu.jdrabner.test.flatrater.FlatData;
import eu.jdrabner.test.flatrater.R;

/**
 * Created by thesh on 01/01/2018.
 */

public class FlatDataAdapter extends ArrayAdapter<FlatData> {

    private ArrayList<FlatData> flats = null;
    private FlatDataWeighting   weighting = null;

    private Context context = null;

    /**
     * Constructor.
     * @param con
     * @param resource
     * @param values
     * @param weight
     */
    public FlatDataAdapter(@NonNull Context con, int resource, ArrayList<FlatData> values, FlatDataWeighting weight) {
        super(con, resource, values);

        flats = values;
        context = con;
        weighting = weight;
    }

    /**
     * Return the view for each flat in the list.
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        FlatData data = flats.get(position);
        View mainView = inflater.inflate(R.layout.layout_flat_selection, parent, false);
        TextView flatAddress = (TextView) mainView.findViewById(R.id.flatAddress);
        flatAddress.setText(data.address);
        TextView flatFloor = (TextView) mainView.findViewById(R.id.flatFloor);
        flatFloor.setText(String.format("Floor: %d", data.floor));
        TextView flatPrice = (TextView) mainView.findViewById(R.id.flatPrice);
        flatPrice.setText(data.price.toString() + " €");
        TextView flatRating = (TextView) mainView.findViewById(R.id.flatRating);
        Double rating = weighting.getRatingForFlat(data);
        flatRating.setText(String.format("%.2f", rating));
        // Do a transition from red to green to yellow
        int finalColor = 0;
        ArgbEvaluator evaluator = new ArgbEvaluator();
        Integer start = Color.parseColor("#ff0000");
        Integer end = Color.parseColor("#00ff00");
        finalColor = (int) evaluator.evaluate((float) (rating / 10.0), start, end);
        flatRating.setBackgroundColor(finalColor);

        // Background color changing for odd entries
        if (position % 2 == 1) {
            mainView.setBackgroundColor(Color.parseColor("#c6c6c6"));
        }

        return mainView;
    }

    /**
     * Update the adapter with new values.
     * @param newFlats  The new values.
     * @param weight    The rating weighting.
     */
    public void update(ArrayList<FlatData> newFlats, FlatDataWeighting weight) {
        flats = newFlats;
        weighting = weight;
        notifyDataSetChanged();
    }
}
