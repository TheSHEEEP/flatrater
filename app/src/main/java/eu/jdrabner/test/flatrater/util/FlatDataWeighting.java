package eu.jdrabner.test.flatrater.util;

import eu.jdrabner.test.flatrater.FlatData;

/**
 * Created by thesheeep on 2.1.2018.
 */

public class FlatDataWeighting {

    public Double minSize = 44.0;
    public Double maxSize = 80.0;

    public Integer sizeWeight = 1;
    public Integer rentDepositWeight = 1;
    public Integer roomsWeight = 1;
    public Integer neighborWeight = 1;
    public Integer wallProtectionWeight = 1;
    public Integer airQualityWeight = 1;
    public Integer liftBigWeight = 1;
    public Integer outsideNoiseWeight = 1;
    public Integer windowNoiseWeight = 1;
    public Integer kitchenEquipmentWeight = 1;
    public Integer socketDistWeight = 1;
    public Integer internetSpeedWeight = 1;
    public Integer busConnectionWeight = 1;
    public Integer storeDistanceWeight = 1;
    public Integer roomLayoutWeight = 1;
    public Integer windowBlindsWeight = 1;
    public Integer surroundingsWeight = 1;

    /**
     * Returns the maximum achievable rating for every flat.
     */
    public Integer getRatingMax() {
        // Add all weights together * 10
        Integer ratingMax = sizeWeight
                + rentDepositWeight
                + roomsWeight
                + neighborWeight
                + wallProtectionWeight
                + airQualityWeight
                + liftBigWeight
                + outsideNoiseWeight
                + windowNoiseWeight
                + kitchenEquipmentWeight
                + socketDistWeight
                + internetSpeedWeight
                + busConnectionWeight
                + storeDistanceWeight
                + roomLayoutWeight
                + windowBlindsWeight
                + surroundingsWeight;
        return ratingMax * 10;
    }

    /**
     * Returns the rating for the passed flat, scaled to a value from 0 to 10.
     * @param data  The flat data to rate.
     */
    public Double getRatingForFlat(FlatData data) {
        Double ratingMax = Double.valueOf(getRatingMax());
        Double rating = 0.0;

        // Size
        Double temp = 0.0;
        if (data.size < minSize) {
            temp = 0.0;
        }
        else if (data.size > maxSize) {
            temp = 10.0;
        }
        else {
            temp = 4.0;
            temp += 6.0 * ((data.size - minSize) / (maxSize - minSize));
        }
        rating += temp * sizeWeight;

        // Rent deposit
        Double minRentD = 250.0;
        Double maxRentD = 2500.0;
        if (data.rentDeposit < minRentD) {
            temp = 10.0;
        }
        else if (data.rentDeposit > maxRentD) {
            temp = 0.0;
        }
        else {
            temp = 0.0;
            temp += 10.0 - 10.0 * ((data.rentDeposit - minRentD) / (maxRentD - minRentD));
        }
        rating += temp * rentDepositWeight;

        // Rooms
        if (data.rooms < 2) {
            temp = 0.0;
        }
        else if (data.rooms == 2) {
            temp = 5.0;
        }
        else if (data.rooms == 3) {
            temp = 7.5;
        }
        else if (data.rooms >= 4) {
            temp = 10.0;
        }
        rating += temp * roomsWeight;

        // Neighbors
        switch (data.numNeighbors) {
            case 0:
                temp = 10.0;
                break;
            case 1:
                temp = 9.0;
                break;
            case 2:
                temp = 7.5;
                break;
            case 3:
                temp = 5.0;
                break;
            case 4:
                temp = 4.0;
                break;
            default:
                temp = 0.0;
        }
        rating += temp * neighborWeight;

        // Wall noise protection
        rating += data.wallNoiseProtection * wallProtectionWeight;

        // Air quality
        rating += data.airQuality * airQualityWeight;

        // Lift big enough
        rating += data.liftRating * liftBigWeight;

        // Outside noise
        rating += (10.0 - data.outsideNoise) * outsideNoiseWeight;

        // Window noise protection
        rating += data.windowNoiseProtection * windowNoiseWeight;

        // Kitchen equipment
        rating += data.kitchenEquipment * kitchenEquipmentWeight;

        // Socket distribution weight
        rating += data.socketDistribution * socketDistWeight;

        // Internet speed
        rating += data.internetSpeed * internetSpeedWeight;

        // Bus connection
        rating += data.busConnection * busConnectionWeight;

        // Store distance
        rating += (10.0 - data.storeDistance) * storeDistanceWeight;

        // Room layout
        rating += data.roomLayout * roomLayoutWeight;

        // Window blinds
        rating += data.windowBlinds * windowBlindsWeight;

        // Surroundings
        rating += data.surroundings * surroundingsWeight;

        // Scale the result to a 0-10 range
        return (rating / ratingMax) * 10.0;
    }
}
