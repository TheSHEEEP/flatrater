package eu.jdrabner.test.flatrater.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

/**
 * Created by thesheeep on 4.1.2018.
 */

public class MultiUseTextWatcher implements TextWatcher {

    private String      symbol = "";
    private Boolean     append = false;
    private EditText    target = null;

    /**
     * Constructor.
     * @param sym   The symbol to add.
     * @param app   If the symbol should be appended (if not, will be a prefix)
     */
    public MultiUseTextWatcher(String sym, Boolean app, EditText tar) {
        symbol = sym;
        append = app;
        target = tar;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        // Append or prepend the symbol
        target.removeTextChangedListener(this);
        String content = target.getText().toString();
        content = content.replace(symbol, "");
        content = content.replace(" ", "");
        if (append) {
            target.setText(content + symbol);
            target.setSelection(content.length());
        }
        else {
            target.setText(symbol + content);
            target.setSelection(symbol.length(), content.length());
        }
        target.addTextChangedListener(this);
    }
}
