package eu.jdrabner.test.flatrater.util;

import android.view.View;
import android.widget.RatingBar;

import java.lang.reflect.Field;

import eu.jdrabner.test.flatrater.FlatRaterContext;

/**
 * Created by thesh on 03/01/2018.
 */

public class RatingWeightChangeListener implements RatingBar.OnRatingBarChangeListener {

    private FlatRaterContext context = null;

    private Field fieldToChange = null;

    /**
     * Constructor
     * @param con       Context.
     * @param fieldName The name of the field to change
     */
    public RatingWeightChangeListener(FlatRaterContext con, String fieldName) {
        context = con;
        try {
            fieldToChange = FlatDataWeighting.class.getField(fieldName);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    /**
     * Store the value and save on change.
     */
    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        try {
            fieldToChange.set(context.weighting, (int)rating);
            context.storeData();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
